��    9      �  O   �      �     �                +     <     J     V     m     �     �     �     �  
   �  
   �     �     �     �     �               "  
   /     :     ?     F  	   X     b     o     ~     �     �     �     �     �     �  	   �  5   �     &     ?     Q     ]     k     �  	   �  	   �     �  !   �     �       
          
   !     ,  
   ;     F     T  S  Y     �	     �	     �	     �	     
     
     0
  !   M
     o
     �
     �
     �
     �
     �
  +   �
                    -     >     Q     f     z     ~     �     �     �     �     �     �     �          )     G     V     n  B   w     �     �     �     �  +     (   /     X     o     �  -   �     �     �     �                 
   "     -     ?     9   !         5   
   3      0   -       2   7       %      (             	      &              /             *                 8   $              ,   4                                   )       6             +      "                     .   '       1            #                     %d venues were created 1 category was created 1 venue was created Add New Category Add New Event Add New Tag Add To Google Calendar Add or remove tags Add or remove venues Address All Categories All Tags All Venues All events Choose from the most used tags City Country Dismiss this notice Edit Category Edit Tag Event Venues Hide dates Hour Minute New Category Name New Event New Tag Name New Venue Name No Venue No categories found No events found No tags found No venues found Parent Category Popular Tags Post Code Schedule end date is before is before the start date. Schedule not recognised. Search Categories Search Tags Search events Separate tags with commas Separate venues with commas Show Less Show More Start date not provided. Start date occurs after end date. State / Province Update Category Update Tag View View Event every %d years every year one time only year Project-Id-Version: Event Organiser
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-05-24 00:20+0100
PO-Revision-Date: 2015-09-25 16:45:15+0000
Last-Translator:  <>
Language-Team: 
Language: Indonesian (Indonesia)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 % lokasi sudah dibuat 1 kategori sudah dibuat 1 lokasi sudah dibuat Tambah Kategori Baru Tambah Agenda Baru Tambah Kata Kunci Baru Tambahkan ke Google Calendar Tambah atau hapus kata-kata kunci Tambah atau hapus tempat/lokasi Alamat Semua Kategori Semua Kata-kata Kunci Semua Tempat/Lokasi Semua Agenda Pilih kata-kata kunci yang sering digunakan Kota Negara Akhiri pemberitahuan ini Sunting Kategori Sunting Kata Kunci Tempat/Lokasi Agenda Sembunyikan tanggal Jam Menit Nama Kategori baru Agenda Baru Kata Kunci Baru Tempat/Lokasi baru Tidak ada tempat/lokasi Kategori tidak ditemukan Agenda tidak ditemukan Kata kunci tidak ditemukan Tempat/Lokasi tidak ditemukan Kategori Induk Kata-kata Kunci Populer Kode Pos Jadwal tanggal selesai adalah sebelum adalah sebelum tanggal mulai Jadwal tidak dikenal Cari Kategori Pencarian Kata-kata Kunci Cari agenda Pisahkan kata kunci (tag) dengan tanda koma Pisahkan tempat/lokasi dengan tanda koma Tampilkan sedikit saja Tampilkan lebih banyak Tanggal mulai tidak diberikan Tanggal mulai terjadi setelah tanggal selesai Propinsi Perbarui Kategori Perbarui Kata Kunci Lihat Lihat Agenda tiap %d tahun tiap tahun sekali waktu saja tahun 